problem:
The specials workflow is sending the email that notifies that HTS step must occur to the wrong person.

fix:
in the workflow database, the Enrollment table correlates user emails with steps.  I can update this table so that HTS is assigned to a different user but first I must add the new user.

These tables are all tightly cross-referenced and there is no documentation so this is not a trivial change.  Furthermore the use of an instance ID makes me think that it is managed by another application.  It makes sense that a new user would require a new instance id and incrementing that manually would require nearly as much work as rewriting the entire application.


--	insert usersmaster
select top 99 
empid
, workflowmasterid
, ntlogin
, email
, uname
from usersmaster

where empid in ( 
59
, 2753
)

select top 99 *
--	update e set empid = 2753
from enrollmentmaster e
where empid in (
59, 
2753
)



problem:
received an email about OCD 78899 related to outstanding errors for EmbTrac 
Error: At least one line item has two designs in the same placement.

usually these emails have subjust similar to 'EmbTrak Process Has Run:'

fix:  if and only if exactly one row is returned from the select query
Use embtrakstage
go

begin tran
update p set error = 0 , flag = 1
--select * 
From PickTicket p
Left Join SOHeaderStage s On p.Code = s.Course_Num Where (Flag = 0 And Error <> 0)
--commit
--rollback


problem:
Hebron Data Transfer report showing canceled pick tickets.  They need to be filtered from the results that generate this report

fix:
on the DCDataTransfer database

update PickTicketHeader
set IgnoreForErrorReport = 1
where pickticketcontrolnumber = '824734611'


problem:
outgoing customer emails had malformed HTML that was resulting in erroneous characters

fix:
I found the error with the SQL command

select * from CustomerEmails with (nolock)
where body like '%</tr<%'

in the ExtPrintHouse database, and updated each row with well-formed HTML

problem:
BAnking records from Wells Fargo have to be uploaded into our document management system.  Archive files containing images and a descriptive .dat file are downloaded and extracted.  Then cb.net/lockback application is run on each directory to generated a formatted text file.  Then the directories can be uploaded into the documentation management system.


problem:
Must update the approval notification list for LPCF process. 

fix:
for some reason this process compares the list of email addresses against hard-coded values for some internal logic.  I am not going to bother trying to understand this, but in order to deal with it the results in [LinePlanChange].[dbo].[EmailAction] must match exactly hard-coded values in \\10.2.0.69\cbnet\applications\ProductPlanning\ApprovalSubmit.asp around line 400

problem: must update the notifications for specials wizard steps

fix: update [Workflow].[dbo].[EMailMaster] to point to the new email addresses

problem:  need to check if orders are in SAP based on shipcustomerreference in idcoheader.

fix: select distinct IHREZ
from PRDSAPDB1.PRD.prd.VBKD
WHERE IHREZ in 
(select top 100 ShipCustomerReference from idocheader with (nolock)
order by OrderInstanceID desc )


problem:  new communicorp products are not being converted from cc product numbers to cb product numbers.

fix: must update the cross reference table from the cc upload table:

begin tran
USE [IdocSO]
GO
INSERT INTO [dbo].[CBCUSTOMERS_InventoryCrossReferenceTable]
           ([AxaptaItemGroup]
           ,[CustomerName]
           ,[CustomerItemNum]
           ,[ItemDescription]
           ,[Vendor#]
           ,[CBItemNum]
           ,[VendorItemDescription]
           ,[AxaptaCostCenter]
           ,[DateCreated])
     Select 
           'CAYS'
           ,'COMMUNICORP'
           , CCBASE
           ,Style
           ,'749'
           ,StyleNum
           ,left(AXDescription, 50)
           ,'70'
           ,getdate() from CommunicorpUpload
		   where AVAILSIZES = 'L' --this may need to be adjusted, only need one entry per product
GO
--commit
--rollback

problem:  User does not have access to sproc

fix: 
grant exec on spgetfollett855details to [cutter&buck\CBEDI-Sec]

CBEDI-Sec is the new group for Karly. 

problem: VRLink failure order has process message "Contains a logo which is missing LogoType or LogoChargeCode information. Logo is for customer 1026609."

fix: this query must return non-null
SELECT [VASChargeCode]
  FROM [CBRepository].[dbo].[CustomerMastLogo]
  where SpServiceMatNumber in (select logomaterial FROM [IdocSO].[dbo].[IDOCDetailLogo]
  where OrderInstanceID = '365351')

problem: invoices failed to print due to no data in database

fix: 

insert databar_base_inv_header (
[InvoiceNum]
, [InvoiceDate]
, [PaymentTo1]
, [PaymentTo2]
, [PaymentTo3]
, [PaymentTo4]
, [PaymentTo5]
, [BillToNumber]
, [BillToLine1]
, [BillToLine2]
, [BillToLine3]
, [BillToLine4]
, [BillToLine5]
, [BillToLine6]
, [BillToCityStZip]
, [BillToCountry]
, [ShipToNumber]
, [ShipToLine1]
, [ShipToLine2]
, [ShipToLine3]
, [ShipToLine4]
, [ShipToLine5]
, [ShipToLine6]
, [ShipToCityStZip]
, [ShipToCountry]
, [SoldTo]
, [Terms]
, [DueDate]
, [PurchaseOrderNum]
, [Weight]
, [OrderNum]
, [FreightTerms]
, [Via]
, [SalesPerson]
, [Total]
, [TaxAmount]
, [Cartons]
, [PODate]
, [PackingList]
, [Currency]
, [ShippingCharges]
, [OutputMode]
, [SubTotal]
, [BillingType]
, [DocTypeId]
, [PlantDescription]
)

SELECT distinct ih.InvoiceNum
              , CONVERT(varchar(10), ih.InvoiceDate, 101) AS InvoiceDate
              , ih.PaymentTo1
              , ih.PaymentTo2
              , ih.PaymentTo3
              , ih.PaymentTo4
              , ih.PaymentTo5
              , ih.BillToNumber
              , ih.BillToLine1
              , ih.BillToLine2
              , ih.BillToLine3
              , ih.BillToLine4
              , ih.BillToLine5
              , ih.BillToLine6
              , ih.BillToCityStZip
              , ih.BillToCountry
              , ih.ShipToNumber
              , ih.ShipToLine1
              , ih.ShipToLine2
              , ih.ShipToLine3
              , ih.ShipToLine4
              , ih.ShipToLine5
              , ih.ShipToLine6
              , ih.ShipToCityStZip
              , ih.ShipToCountry
              , ih.SoldTo
              , ih.Terms
              , CONVERT(varchar(10), ih.DueDate, 101) AS DueDate
              , ih.PurchaseOrderNum
              , ISNULL(ih.Weight, 0.0) AS Weight
              , OrderNum = case isnumeric(ih.Ordernum ) 
                     when 1 then CAST(CAST(ih.OrderNum AS bigint) AS varchar(10))
                     else ih.ordernum
                     end
              , ih.FreightTerms
              , ih.Via
              , ih.SalesPerson
              , ih.Total
              , ih.TaxAmount
              , ih.Cartons
              , CONVERT(varchar(10), ih.PODate, 101) AS PODate
              , ih.PackingList
              , ih.Currency
              , ISNULL(ih.ShippingCharges, 0) AS ShippingCharges
              , ih.OutputMode
              , ISNULL(ih.SubTotal, 0) AS SubTotal
              , ih.BillingType
              , CAST(1 AS int) AS DocTypeId 
              , plantdescription = case plant
                     when '0020' then 'Hebron, KY'
                     else 'Renton, WA'
                     end
FROM   dbo.SAPInvoiceHeader ih
join cbrepository.dbo.invoicedetail id on ih.invoicenum = id.billingdoc
LEFT OUTER JOIN
              dbo.vw_zero_units_invoices 
ON            ih.InvoiceNum = dbo.vw_zero_units_invoices.InvoiceNum
WHERE  (
(dbo.vw_zero_units_invoices.InvoiceNum IS NULL) 
OR            (NOT (dbo.vw_zero_units_invoices.InvoiceNum IS NULL)) 
AND           (dbo.vw_zero_units_invoices.total_units <> 0)
)
and ih.invoicenum in (

��,
��

)


problem: Alert email with the subject EmbTrak Process Has Run: BatchID needs picktickets canceled.

fix: on prd-sql2k5-01 run
 update p set error = 0 , flag = 1
From PickTicket p
Left Join SOHeaderStage s On p.Code = s.Course_Num 
Where (Flag = 0 And Error <> 0)

problem:  Outlet Store - PT# 82681033 10/24 $4,535.96

Hi Jonathan,

Please process PT# 82681033 10/24 $4,535.96  to Retail Pro, thanks. 

fix: \\svr-fps-na1\dept\it\Private\Documentation\Outlet Store Pickticket Induction.docx 

problem: blank lines in PIX inventory files

fix: SELECT Reference42
  FROM [DCDataTransfer].[dbo].[PIX]pix
				Inner Join AdjustmentCompare
					On Pix.TransactionNumber = AdjustmentCompare.TransactionNumber
  where DataToFile = 'PX50024935.DAT'  
  order by pixid desc

from  DCDataTransfer.dbo.vwGetAdjustmentPix1

			Case
				When Reference42 = 'QH' Then 'QH'
				When Reference42 In('NO', 'LW', 'LC') Then 'BL'
			End


problem: outgoing emails failing with error Database Mail Attachment transfer failed

solution:  empty the attachment tables with the job CBReportMailer Attachment Purge

problem:  need to cancel EDT work orders

solution:

  begin tran
  update [edt].[dbo].[WorkRequests]
  set Complete = 1 
  where WorkRequestID between 122727 and 122805
  --where OrderNumber = '2515963' 
  --commit
  --rollback


problem:  need to regenerate a QRS file

solution:

This view contains the data that goes into a QRS file
SELECT *
  FROM [PartNumberGenerator].[dbo].[vwGetQRSDataForOutput]

to get the upcs in question into that file, generate it, and set the upc status back, use the following three statements

  update [PartNumberGenerator].[dbo].[mutHeaders]
  set QRS = 0
  where mutHeaderID in (
	  SELECT distinct mutHeaderID
	  FROM [PartNumberGenerator].[dbo].[mutDetails]
	  where upc in ('UPC in question')
	  )

EXEC [PartNumberGenerator].[dbo].[mspGetQRSFile]

  update [PartNumberGenerator].[dbo].[mutHeaders]
  set QRS = 1
  where mutHeaderID in (
	  SELECT distinct mutHeaderID
	  FROM [PartNumberGenerator].[dbo].[mutDetails]
	  where upc in ('UPC in question')
	  )


issue: EDT automation went crazy and created a bunch of work requests.  

fix: based on the order number run 

  begin tran --commit --rollback

  delete 
  FROM [edt].[dbo].[Statuses]
  where WorkRequestID in 
  (select WorkRequestID from [edt].[dbo].[WorkRequests] where  OrderNumber = '2572039')

  delete 
  FROM [edt].[dbo].[WorkRequests]
  where OrderNumber = '2572039'


problem: EDI group needs database access
 fix:
grant exec on [dbo].[GetNextControlNumbers] to [cutter&buck\cbedi-sec]

problem:  Karly needs to know which logos are associated with an order
fix: send her the results of this query
SELECT distinct [LogoMaterial]
  FROM [IdocSO].[dbo].[IDOCDetailLogo]
  where OrderInstanceID in (
  select OrderInstanceID from IdocSO.dbo.IDOCHeader where ShipCustomerReference = 'GET THIS NUMBER FROM KARLY'


problem:  Rich needs to give someone access to his Commissions custom app
fix: USE [cbnetempdata]
GO

INSERT INTO [dbo].[ApplicationAccess]
           ([ApplicationID]
           ,[EmpID]
           ,[AccessLevel]
           ,[AccessDescription])
     VALUES
           (7
           ,2900
           ,1
           ,'commissions admin')
GO

problem: VAS Bundle 2 day extension to cancel dates

fix: add the SoldTo account numbers to the table [dbo].[VASExtendedCancelDateCustomers]


problem: need to add a new prefix to the EDT access application

fix: The application is located at \\prd-appiis-01\EmbroideryDevelopment2\EDT.mdb
Simply open this file and proceed to the design view of frmWorkRequestEditLogo.  View the properties of the prefix drop down and append ;U;Underlay if that is the new spec


problem: VRLink bulk insert exception thrown

fix: Once you have verified that orders are missing by checking the batch count against the PDF for that batch found in \\SVR-VRLINK-02\hqbox\Archive you must delete the entire batch using the sproc [IdocSO].[dbo].[spDeleteBatchFromIdocs].  At that point you can move the all files from the archive that arrived at that time back up one folder making sure not to overwrite any files that may have been added there by the live VRLink induction process.  The next pass by OrderPump should catch these files and induct the orders.




problem: locate pick ticket numbers that had logo developers in embtrak but no rows were updated when logo developer field was set

fix:
  SELECT *
  FROM [dbatools].[dbo].[EventLog]
  where SystemCode = 500 and SubsystemCode = 10

problem: need an xpath query to search snap file from Hebron:

fix: //InventorySnapshot[SizeDesc = '3XT'] [Style = 'BCO00842'] [Color = 'BL']


problem: need to quickly search EmbTrak comment file for malformed lines that have too many tabs in them

fix: run this powershell to identify the malformed lines:
foreach ($line in [System.IO.File]::ReadLines("\\svrsapftp\prd\EMBTrak\OUT\XFER\embtrak_ord_cmt.txt"))
{ 
	if ($line.Split('\t').Count -gt 6)
	{
		echo ($line)
	}
}